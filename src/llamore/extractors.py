import logging
from abc import ABC, abstractmethod
from typing import Dict, List, Literal, Optional, Union

import huggingface_hub
import ollama
from openai import OpenAI

from .prompter import (
    BasePrompter,
    InstructionPrompter,
    NuExtractPrompter,
    SchemaPrompter,
)
from .reference import Reference

_LOGGER = logging.getLogger(__name__)


class BaseExtractor(ABC):
    def __init__(self, prompter: BasePrompter):
        self._prompter = prompter

    def __call__(self, input_text: str, **kwargs) -> List[Reference]:
        """Extract the references from the input text.

        Args:
            input_text: The input text from which to extract the references.
            kwargs: Passed on to the model call.

        Returns:
            A list of References.
        """
        prompt = self._prompter.prompt(input_text)
        _LOGGER.debug(f"Prompt: {prompt}")

        response = self._prompt_model(prompt, **kwargs)
        _LOGGER.debug(f"Response: {response}")

        references = self._prompter.parse(response)

        return references

    @abstractmethod
    def _prompt_model(
        self,
        prompt: Union[str, List[Dict[str, str]]],
        **kwargs,
    ) -> str:
        """Prompt the model and return the response.

        Args:
            prompt: The prompt for the model.
            kwargs: Passed on to the model call.

        Returns:
            The response of the model.
        """
        ...


class OllamaExtractor(BaseExtractor):
    """Uses an Ollama endpoint to extract the references.

    Args:
        model: The model to use for the extraction.
        kwargs: kwargs are passed on to `ollama.Client`
    """

    def __init__(
        self,
        model: str = "llama3.1:8b",
        **kwargs,
    ):
        super().__init__(prompter=InstructionPrompter())

        self._model = model
        self._client = ollama.Client(**kwargs)

        self.load_model()

    def load_model(self):
        """Pull and load the model into memory (CPU or GPU)."""
        available_models = [model["model"] for model in self._client.list()["models"]]
        if self._model not in available_models:
            _LOGGER.info("Pulling model ...")
            self._client.pull(self._model)

        loaded_models = [model["name"] for model in self._client.ps()["models"]]
        if self._model not in loaded_models:
            _LOGGER.info("Loading model ...")
            self._client.generate(self._model)

    def _prompt_model(self, prompt: List[Dict[str, str]], **kwargs) -> str:
        output = self._client.chat(
            model=self._model,
            messages=prompt,
            options=kwargs,
        )
        response = output["message"]["content"]

        return response


class OpenaiExtractor:
    """Use an OpenAI endpoint to extract the references.

    We also use this extractor with compatible OpenAI endpoints provided by SGLang, Ollama, TGI, etc.
    In this case you do not need to provide an `api_key` and/or `model`.

    Args:
        api_key: Your OpenAI API key.
        model: The model to use for the extraction.
        prompter: The prompter to use for the extraction.
        endpoint: The endpoint to use for the extraction.
        kwargs: kwargs are passed on to `openai.OpenAI`
    """

    def __init__(
        self,
        api_key: str = "None",
        model: str = "None",
        prompter: Optional[BasePrompter] = None,
        endpoint: Literal["create", "parse"] = "create",
        **kwargs,
    ):
        self._prompter = prompter or SchemaPrompter()
        self._endpoint = endpoint

        self._client = OpenAI(api_key=api_key, **kwargs)
        self._model = model

    def __call__(self, input_text: str, **kwargs) -> List[Reference]:
        """Extract the references from the input text.

        Args:
            input_text: The input text from which to extract the references.
            kwargs: Passed on to the model call.

        Returns:
            A list of References.
        """
        prompt = self._prompter.prompt(input_text)
        _LOGGER.debug(f"Prompt: {prompt}")

        if self._endpoint == "create":
            references = self._call_create_endpoint(prompt, **kwargs)
        elif self._endpoint == "parse":
            references = self._call_parse_endpoint(prompt, **kwargs)
        else:
            raise ValueError(f"Invalid endpoint: {self._endpoint}")

        return references

    def _call_create_endpoint(
        self, prompt: List[Dict[str, str]], **kwargs
    ) -> List[Reference]:
        if (
            isinstance(self._prompter, SchemaPrompter)
            and "response_format" not in kwargs
        ):
            kwargs["response_format"] = {
                "type": "json_schema",
                "json_schema": {
                    "name": "References",
                    "schema": self._prompter.json_schema,
                },
            }

        output = self._client.chat.completions.create(
            model=self._model, messages=prompt, **kwargs
        )
        _LOGGER.debug(f"Output: {output}")

        references = self._prompter.parse(output.choices[0].message.content)

        return references

    def _call_parse_endpoint(
        self, prompt: List[Dict[str, str]], **kwargs
    ) -> List[Reference]:
        if (
            isinstance(self._prompter, SchemaPrompter)
            and "response_format" not in kwargs
        ):
            kwargs["response_format"] = self._prompter.schema_model

        output = self._client.beta.chat.completions.parse(
            model=self._model, messages=prompt, **kwargs
        )
        try:
            references: List[Reference] = output.choices[0].message.parsed.references
        except Exception as e:
            _LOGGER.debug(f"Error parsing response: {e}")
            references = []

        if output.choices[0].message.refusal:
            _LOGGER.debug(f"Refusal: {output.choices[0].message.refusal}")

        return references


class TgiExtractor(BaseExtractor):
    """Use an TGI endpoint to extract references.

    Here the model is already defined when providing the endpoint.
    TODO: For now, it only works with the NuExtract model!

    Args:
        kwargs: Passed on to the `huggingface_hub.InferenceClient`.
    """

    def __init__(self, **kwargs):
        super().__init__(prompter=NuExtractPrompter())

        self._client = huggingface_hub.InferenceClient(**kwargs)

    def _prompt_model(self, prompt: str) -> str:
        output = self._client.text_generation(prompt, details=True, max_new_tokens=1000)

        response = output.generated_text

        return response
