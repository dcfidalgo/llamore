from typing import Any, Dict, List, Union

import numpy as np
from pydantic import BaseModel
from rapidfuzz.distance import Levenshtein
from scipy.optimize import linear_sum_assignment

from llamore.reference import Organization, Person


class F1:
    """Compare predictions and labels and compute a F1 score.

    Args:
        max_distance: Maximum Levenshtein distance, which still counts as a match.
            Set this to 0 to require an exact match.
    """

    def __init__(self, max_distance: int = 0):
        self.max_distance = max_distance

    def compute_macro_average(
        self, predictions: List[List[BaseModel]], labels: List[List[BaseModel]]
    ) -> float:
        """Compute the macro average of the f1 scores between the predictions and the labels.

        Args:
            predictions:
            labels:

        Returns:
            The macro average f1 score.
        """
        f1_scores = []

        for prediction, label in zip(predictions, labels):
            f1_score = self.compute(prediction, label)
            if isinstance(f1_score, float):
                f1_score = [f1_score]
            f1_scores += f1_score

        return float(np.mean(f1_scores))

    def compute(
        self,
        prediction: Union[BaseModel, List[BaseModel]],
        label: Union[BaseModel, List[BaseModel]],
    ) -> Union[float, List[float]]:
        """Compute the F1s and match the most similar predictions and labels.

        Args:
            predictions:
            labels:

        Returns:
            The f1 score(s).
        """
        # Special definition of undefined F1 ...
        if prediction == label == []:
            return 1.0

        if not isinstance(prediction, list):
            prediction = [prediction]
        if not isinstance(label, list):
            label = [label]
        result = self._compute_f1s(prediction, label)

        if len(result) == 1:
            return result[0]
        return result

    def _compute_f1s(
        self, prediction: List[BaseModel], label: List[BaseModel]
    ) -> List[float]:
        """Compute the F1s and match the most similar predictions and labels.

        Args:
            predictions:
            labels:

        Returns:
            The f1 scores.
        """
        f1_matrix = np.zeros((len(prediction), len(label)))
        for i, pred in enumerate(prediction):
            for j, lab in enumerate(label):
                f1_matrix[i, j] = self._compute_f1(pred, lab)

        idx = linear_sum_assignment(f1_matrix * -1)
        f1s = [float(f1) for f1 in f1_matrix[idx]]

        # add zeros for missing/extra references
        if f1_matrix.shape[0] != f1_matrix.shape[1]:
            f1s += [0.0 for _ in range(np.abs(f1_matrix.shape[0] - f1_matrix.shape[1]))]

        return f1s

    def _compute_f1(self, prediction: BaseModel, label: BaseModel) -> float:
        """Compute the F1 score between a predicted and a gold label reference."""
        # count matches
        total_matches = self._count_matches(prediction, label)

        # count predictions
        n_predictions = self._count_not_nones(prediction)

        # count labels
        n_labels = self._count_not_nones(label)

        return precision_recall_f1(total_matches, n_labels, n_predictions)["f1"]

    def _count_matches(
        self,
        prediction: Union[None, str, int, List, BaseModel],
        label: Union[None, str, int, List, BaseModel],
    ) -> int:
        """Recursively counts the matches between two BaseModels.

        Args:
            prediction: The prediction.
            label: The gold label.

        Return:
            Number of matches.

        Raises:
            TypeError if prediction and label are not the same type (except for Nones).
        """
        if prediction is None or label is None:
            return 0

        if isinstance(prediction, BaseModel):
            # Check if mixing Person and Organization
            if (
                isinstance(prediction, (Person, Organization))
                and isinstance(label, (Person, Organization))
                and type(prediction) is not type(label)
            ):
                return 0

            self._check_for_same_type(prediction, label)

            tot_matches = 0
            for field, info in prediction.model_fields.items():
                if info.exclude:
                    continue
                tot_matches += self._count_matches(
                    getattr(prediction, field),
                    getattr(label, field),
                )
            return tot_matches

        if isinstance(prediction, list):
            self._check_for_same_type(prediction, label)

            match_matrix = np.zeros((len(prediction), len(label)), dtype=np.int32)
            for i, pred in enumerate(prediction):
                for j, lab in enumerate(label):
                    match_matrix[i, j] = self._count_matches(pred, lab)

            if match_matrix.sum() == 0:
                return 0

            idx = linear_sum_assignment(match_matrix * -1)
            matches = match_matrix[idx].sum()

            return int(matches)

        if isinstance(prediction, str):
            self._check_for_same_type(prediction, label)

            if self.max_distance == 0:
                if prediction == label:
                    return 1
                return 0

            distance = Levenshtein.distance(
                prediction, label, score_cutoff=self.max_distance
            )
            if distance <= self.max_distance:
                return 1
            return 0

        else:
            raise TypeError(f"Type '{type(prediction)}' not supported!")

    @staticmethod
    def _check_for_same_type(prediction: Any, label: Any):
        if type(prediction) is not type(label):
            raise TypeError(
                f"Trying to compare different types: '{type(prediction)}' (prediction) and '{type(label)}' (label)"
            )

    def _count_not_nones(
        self,
        value: Union[None, str, int, List, BaseModel],
    ) -> int:
        if value is None:
            return 0

        if isinstance(value, (str, int)):
            return 1

        if isinstance(value, BaseModel):
            tot_not_nones = 0
            for field, info in value.model_fields.items():
                if info.exclude:
                    continue
                tot_not_nones += self._count_not_nones(getattr(value, field))
            return tot_not_nones

        if isinstance(value, list):
            tot_not_nones = 0
            for v in value:
                tot_not_nones += self._count_not_nones(v)
            return tot_not_nones


def compute_coarse_f1(
    predictions: List[List[BaseModel]], labels: List[List[BaseModel]]
) -> Dict[str, float]:
    """Compute a coarse F1 score, in which we only check if the entire references match exactly.

    Args:
        predictions:
        labels:

    Returns:
        A dict with the keys: precision, recall, f1
    """
    if len(predictions) != len(labels):
        raise ValueError(
            f"`predictions` and `labels` must have the same length: {len(predictions)} vs. {len(labels)}"
        )

    complete_matches = 0

    for prediction, label in zip(predictions, labels):
        for pred in prediction:
            complete_matches += pred in label

    n_predictions = sum([len(prediction) for prediction in predictions])
    n_labels = sum([len(label) for label in labels])

    return precision_recall_f1(complete_matches, n_labels, n_predictions)


def precision_recall_f1(
    matches: int, labels: int, predictions: int
) -> Dict[str, float]:
    """Compute the precision, recall and F1.

    Args:
        matches: Number of matches.
        labels: Number of labels or label fields, that are not None.
        predictions: Number of predictions or prediction fields, that are not None.

    Returns:
        A dict with 'precision', 'recall' and 'f1'.
    """
    precision = matches / (predictions or 1)
    recall = matches / (labels or 1)
    f1 = 2 * precision * recall / ((precision + recall) or 1)

    return {"precision": precision, "recall": recall, "f1": f1}
