import json
import logging
import re
from abc import ABC, abstractmethod
from typing import Dict, List, Union

from pydantic import BaseModel, Field, ValidationError, create_model
from pydantic.json_schema import GenerateJsonSchema

from llamore.reference import Reference

_LOGGER = logging.getLogger(__name__)


class BasePrompter(ABC):
    @abstractmethod
    def prompt(self, input_text: str) -> Union[str, List[Dict[str, str]]]:
        """Return the prompt to extract References from the input text.

        Args:
            input_text: The input text.

        Returns:
            The prompt as string (text generation) or as messages (chat completion).
        """
        ...

    @abstractmethod
    def parse(self, response: str) -> List[Reference]:
        """Parse the response of the LLM.

        Args:
            response: The response of the LLM.

        Returns:
            A list of References.

        """
        ...


class InstructionPrompter(BasePrompter):
    """Prompts instruction models for JSON formatted output with messages.

    Args:
        system_in_user_prompt: Add the system prompt to the user prompt in the messages?
    """

    def __init__(self, system_in_user_prompt: bool = False):
        self._system_in_user_prompt = system_in_user_prompt

    @property
    def json_schema(self) -> str:
        return Reference.model_json_schema(
            mode="serialization", schema_generator=GenerateJsonSchemaNoTitle
        )

    def _system_prompt(self) -> str:
        """The system prompt for extracting References in json format."""
        return (
            "You are an expert in scholarly references and citations. "
            "You help the user to extract citation data from scientific works."
        )

    def _user_prompt(self, text: str) -> str:
        """The user prompt.

        Args:
            text: The input text from which to extract the structured information.

        Returns:
            The prompt for the user role.
        """
        prompt = (
            "Extract all references from the given text. Output the references in JSON format with following schema:"
            f"\n\n{self.json_schema}\n\n"
            "Output the references as JSON formatted strings, each reference in a new line. "
            "Only output the JSON strings, nothing else. Don't use markdown.\n\n"
            f"TEXT: <<<{text}>>>"
        )

        return prompt

    def prompt(self, input_text: str) -> List[Dict[str, str]]:
        """Generate the prompt messages for the LLM to extract References.

        Args:
            input_text: The input text from which to extract References.

        Returns:
            A list of messages that consist of the `role` and the `content`.
        """
        system_prompt = self._system_prompt()
        user_prompt = self._user_prompt(input_text)

        return self._to_messages(system_prompt, user_prompt)

    def _to_messages(
        self, system_prompt: str, user_prompt: str
    ) -> List[Dict[str, str]]:
        if self._system_in_user_prompt:
            messages = [
                {
                    "role": "user",
                    "content": f"{system_prompt}\n\n{user_prompt}",
                }
            ]
        else:
            messages = [
                {"role": "system", "content": system_prompt},
                {"role": "user", "content": user_prompt},
            ]

        _LOGGER.debug(f"MESSAGES: {messages}")

        return messages

    def parse(self, response: str) -> List[Reference]:
        """Parse the LLM response in json format to a list of data model instances.

        We post-process the LLM response line by line and pass it on to the data model.

        Args:
            response: The response of the LLM in json format.

        Returns:
            A list of data model instances.
        """
        # TODO: Common practice is also to repeatedly prompt the LLM when failing to parse its response.
        data_model = Reference
        data_model_instances = []

        response = response.strip()

        # Parse line by line, everything between curly brackets
        prog = re.compile("{.*}")
        for line in response.split("\n"):
            match = prog.search(line)
            if match is not None:
                try:
                    instance = data_model.model_validate_json(match.group())
                except ValidationError as err:
                    _LOGGER.debug(f"{err}, {match.group()}")
                else:
                    if instance != Reference():
                        data_model_instances.append(instance)

        # Try to parse the response as is (bigger models are too smart ...)
        if not data_model_instances:
            # remove markdown
            if response.startswith("```"):
                response = "\n".join([line for line in response.split("\n")][1:-1])
            try:
                response_json = json.loads(response)
            except json.JSONDecodeError as err:
                _LOGGER.debug(err)
            else:
                if isinstance(response_json, dict):
                    response_json = [response_json]
                for instance_json in response_json:
                    try:
                        instance = data_model(**instance_json)
                    except ValidationError as err:
                        _LOGGER.debug(err)
                    else:
                        if instance != Reference():
                            data_model_instances.append(instance)

        return data_model_instances


class NuExtractPrompter(BasePrompter):
    """DEPRECATED! Prompts the NuExtract model for JSON formatted output with messages."""

    @staticmethod
    @property
    def _template_references() -> str:
        template = {
            "references": [
                {
                    "analytic_title": "",
                    "monographic_title": "",
                    "journal_title": "",
                    "authors": [],
                    "editors": [],
                    "date": "",
                    "publication_place": "",
                    "volume": "",
                    "issue": "",
                }
            ]
        }
        return json.dumps(template, indent=4)

    def prompt(self, input_text: str) -> str:
        return self._prompt(text=input_text, template=self._template_references)

    @staticmethod
    def _prompt(text: str, template: str) -> str:
        return (
            f"""<|input|>\n### Template:\n{template}\n### Text:\n{text}\n\n<|output|>"""
        )

    @staticmethod
    def parse(response: str) -> List[Reference]:
        references = []

        try:
            references = json.loads(response)["references"]
        except (json.JSONDecodeError, KeyError) as err:
            _LOGGER.debug(response)
            _LOGGER.debug(err)
            references = []

        for ref in references:
            try:
                reference = Reference(**ref)
            except ValidationError as err:
                _LOGGER.debug(err)
            else:
                if reference != Reference():
                    references.append(reference)

        return references


class SchemaPrompter(BasePrompter):
    """Prompts instruction models for JSON formatted output with messages.

    Args:
        system_in_user_prompt: Add the system prompt to the user prompt in the messages?
        print_pretty: Print the response pretty?
        step_by_step: Extract the references step by step?
    """

    def __init__(
        self,
        system_in_user_prompt: bool = False,
        print_pretty: bool = True,
        step_by_step: bool = False,
    ):
        self._system_in_user_prompt = system_in_user_prompt
        self._print_pretty = print_pretty
        self._step_by_step = step_by_step

        if self._step_by_step:
            self._schema_model = create_model(
                "References",
                steps=(
                    List[str],
                    Field(
                        description="A list of necessary steps to extract all the references."
                    ),
                ),
                references=(List[Reference], Field(description="A list of references")),
            )
        else:
            self._schema_model = create_model(
                "References",
                references=(List[Reference], Field(description="A list of references")),
            )

    @property
    def schema_model(self) -> type[BaseModel]:
        return self._schema_model

    @property
    def json_schema(self) -> str:
        return self._schema_model.model_json_schema(
            mode="serialization", schema_generator=GenerateJsonSchemaNoTitle
        )

    def _system_prompt(self) -> str:
        """The system prompt for extracting References in json format."""
        return (
            "You are an expert in scholarly references and citations. "
            "You help the user to extract citation data from scientific works."
        )

    def _user_prompt(self, text: str) -> str:
        """The user prompt.

        Args:
            text: The input text from which to extract the structured information.

        Returns:
            The prompt for the user role.
        """
        prompt = (
            "Extract all references from the given text. Output the references in JSON format with following schema:"
            f"\n\n{self.json_schema}\n\n"
            f"{'Extract the references step by step. ' if self._step_by_step else ''}"
            f"Only output the JSON string, nothing else. {'Print it pretty. ' if self._print_pretty else ''}"
            "Don't use markdown.\n\n"
            f"TEXT: <<<{text}>>>"
        )

        return prompt

    def _to_messages(
        self, system_prompt: str, user_prompt: str
    ) -> List[Dict[str, str]]:
        """Convert the system and user prompts to messages.

        Args:
            system_prompt: The system prompt.
            user_prompt: The user prompt.

        Returns:
            A list of messages that consist of the `role` and the `content`.
        """
        if self._system_in_user_prompt:
            messages = [
                {
                    "role": "user",
                    "content": f"{system_prompt}\n\n{user_prompt}",
                }
            ]
        else:
            messages = [
                {"role": "system", "content": system_prompt},
                {"role": "user", "content": user_prompt},
            ]

        _LOGGER.debug(f"MESSAGES: {messages}")

        return messages

    def prompt(self, input_text: str) -> List[Dict[str, str]]:
        """Generate the prompt messages for the LLM to extract References.

        Args:
            input_text: The input text from which to extract References.

        Returns:
            A list of messages that consist of the `role` and the `content`.
        """
        system_prompt = self._system_prompt()
        user_prompt = self._user_prompt(input_text)

        return self._to_messages(system_prompt, user_prompt)

    def parse(self, response: str) -> List[Reference]:
        try:
            references = self.schema_model.model_validate_json(response).references
        except ValidationError as e:
            _LOGGER.debug(f"ValidationError: {e}")
            references = []

        references = [ref for ref in references if ref != Reference()]

        return references


class GenerateJsonSchemaNoTitle(GenerateJsonSchema):
    def generate(self, schema, mode="validation"):
        json_schema = super().generate(schema, mode=mode)
        if "title" in json_schema:
            del json_schema["title"]
        return json_schema

    def get_schema_from_definitions(self, json_ref):
        json_schema = super().get_schema_from_definitions(json_ref)
        if json_schema and "title" in json_schema:
            del json_schema["title"]
        return json_schema

    def field_title_should_be_set(self, schema) -> bool:
        return False
