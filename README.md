# Llamore

***L**arge **LA**nguage **MO**dels for **R**eference **E**xtraction*

Idea: A framework to extract and evaluate references from free-form text using LLMs.

## Setup

```bash
pip install git+https://gitlab.mpcdf.mpg.de/dcfidalgo/llamore.git
```

## Dev setup on MPCDF's RVS

Go to https://rvs.mpcdf.mpg.de/rv/submit and submit a new "Jupyter for Machine Learning" (Generic Jupyter Lab) session on Raven.

Once you are connected to your Jupyter Lab session, open a terminal and clone the repo:
```bash
git clone https://gitlab.mpcdf.mpg.de/dcfidalgo/llamore.git
cd llamore
```

Load the newest anaconda module, create a virtual environment, and activate it:
```bash
module load anaconda/3/2023.03
python -m venv .venv --system-site-packages
source .venv/bin/activate
```

Now install the package in editable mode (for development) and create a kernel for Jupyter:
```bash
pip install -e .
python -m ipykernel install --user --name llamore --display-name "Python (llamore)"
```

Now, if you open a notebook or create a new one, you can choose the "Python (llamore)" kernel and work with *llamore*.

### Run ollama with apptainer

Open a terminal in your Jupyter Lab, load the *apptainer* module, and pull the *ollama* image from DockerHub:
```bash
module load apptainer/1.3.2
apptainer pull docker://ollama/ollama
```

Now you can run the ollama server with
```bash
apptainer run --nv ollama_latest.sif
```

Leave the terminal open and switch to a notebook with the "Python (llamore)" kernel.
Check that you can connect to the server by simply listing all the downloaded models.
It should be an empty list the first time.
```python
import ollama
ollama.list()
# {'models': []}
```

## Run tests / notebooks

Make sure you have following environment variables set via an `.env` file (some or all of them):

```bash
OLLAMA_HOST=...
SGLANG_HOST=...
TGI_URL=...
OPENAI_API_KEY=...
GEMINI_API_KEY=...
ARGILLA_API_KEY=...
ARGILLA_URL=...
```

The Python extension of VSCode should automatically detect the `.env` file and load the environment variables.
