import pytest
from llamore.reference import Organization, Person, Reference


def test_reference():
    json_str = (
        """{"authors": [{"forename": "forename  check\\t", "surname": "surname"}]}"""
    )
    assert Reference.model_validate_json(json_str).authors == [
        Person(forename="forename check", surname="surname")
    ]

    json_str = """{"date": "", "analytic_title": "title"}"""
    ref = Reference.model_validate_json(json_str)
    assert ref.publication_date is None

    json_str = """{"authors": [{"name": ""}]}"""
    ref = Reference.model_validate_json(json_str)
    assert ref.authors is None


def test_authitor():
    p = Person(forename="", surname="Check")
    assert p.forename is None
    assert p.surname == "Check"

    p = Person(forename="Check\t", surname="")
    assert p.forename == "Check"
    assert p.surname is None

    o = Organization(name=" GmbH\t")
    assert o.name == "GmbH"
    o = Organization(name=123)
    assert o.name == "123"


@pytest.mark.parametrize(
    "inp,out", [(1988, "1988"), (1.8, "1.8"), (None, None), ("None", "None")]
)
def test_date(inp, out):
    c = Reference(analytic_title="title", publication_date=inp)
    assert c.publication_date == out
