from llamore.extractors import OllamaExtractor, OpenaiExtractor
from llamore.prompter import InstructionPrompter, SchemaPrompter


def test_ollama_extractor(monkeypatch):
    def patch_load_model(self):
        return

    monkeypatch.setattr(OllamaExtractor, "load_model", patch_load_model)

    extractor = OllamaExtractor(model="llama3.3:70b")

    assert isinstance(extractor._prompter, InstructionPrompter)
    assert extractor._model == "llama3.3:70b"


def test_openai_extractor():
    extractor = OpenaiExtractor(api_key="api_key")

    assert isinstance(extractor._prompter, SchemaPrompter)
    assert extractor._model == "None"
