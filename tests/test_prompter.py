from llamore.prompter import InstructionPrompter


# @pytest.mark.skip
def test_instruction_prompter():
    json_prompter = InstructionPrompter()
    messages = json_prompter.prompt("a test foot note!")
    assert messages[0]["content"]
    assert messages[1]["content"]
