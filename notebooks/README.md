# Notebooks

## Setup

You need some env variables to run the notebooks.
Following two methods are convenient to load env variables in notebooks.

### `.env` file

```bash
pip install python-dotenv
```

andb create an `.env` file in the notebook directory with

```bash
OLLAMA_HOST=...
TGI_URL=...
ARGILLA_URL=...
ARGILLA_API_KEY=...
OPENAI_API_KEY=...
```

### jupyter kernel spec

Create a kernelspec before installing it via
```bash
ipython kernel install --name llamore --prefix /tmp
```

Edit the `kernel.json` to include the necessary env variables
```bash
vim /tmp/share/jupyter/kernels/llamore/kernel.json
```
```json
...
 "env": {
  "OLLAMA_HOST": "...",
  "TGI_URL": "...",
  "ARGILLA_URL": "...",
  "ARGILLA_API_KEY": "...",
  "OPENAI_API_KEY": "..."
 }
...
```

Install the kernel via
```bash
jupyter kernelspec install --user /tmp/share/jupyter/kernels/llamore
```
